function TrunkModel( provider, technology, channel, event ){
	this.provider = provider;
	this.technology = technology;
	this.channel = channel;
	this.event = typeof event !== 'undefined'? event : 'CHANNEL_START';
};

TrunkModel.prototype = {
	disconnectTrunk: function(t_on, t_off){
		if(Math.floor(t_on.length * 0.20) >= t_off.length ){
			var selectedTrunk;
			var selectedTrunksArr = [];
			var tOffArr = [];
			var jump_down = t_on.length > 10 ? Math.floor((Math.random() * 10)+1) : 1;
			// console.log(jump_down);

			for (var i = 0; i < jump_down; i++){
				do {
					selectedTrunk = Math.floor(Math.random() * t_on.length)
				} while (t_on[selectedTrunk].event == 'CHANNEL_END');

				selectedTrunksArr.push(selectedTrunk);

				tOffArr.push(new TrunkModel(
					t_on[selectedTrunk].provider,
					t_on[selectedTrunk].technology,
					t_on[selectedTrunk].channel,
					'CHANNEL_END'
				));
			}
			// console.log(tOffArr);
			return {
				trunk: tOffArr, 
				index: selectedTrunksArr
			};
		}else{
			return null;
		}
	}
};

module.exports = TrunkModel;