var redis = require('redis'),
    redisClient = redis.createClient();
var amqp = require('amqp'),
    connection = amqp.createConnection({host: 'localhost'});

connection.on('ready', function(){

    connection.queue('trunks.queue', function(q){
        q.bind('trunk.#');

        q.subscribe(function(message){

            var trunks = message;
            // console.log(trunks);

            redisClient.on("error", function(err){
                console.log("Error: " + err);
            });

            redisClient.set("trunks", JSON.stringify(trunks));
        });
    });
});