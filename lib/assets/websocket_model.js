var http = require('http');
var sockjs = require('sockjs');
var websocket;

websocket = {
    connectSock: function( redisClient ){
        // console.log(redisClient);
        var sock = sockjs.createServer();
        sock.on('connection', function(conn) {

            redisClient.on("error", function(err){
                console.log("Error: " + err);
            });

            setInterval( function() {
                redisClient.keys("trunk_*", function (err, keys){
                    var trunks_list = [];
                    
                    for(var i = 0; i < keys.length; i++){
                        redisClient.get(keys[i], function (err, trunks ) {
                            trunks_list.push(trunks);
                            sendMessage(trunks);
                            // console.log(trunks_list);
                        });    
                    }
                })
            }, 3000 );

            function sendMessage (trunks_list){
                conn.write(trunks_list);
                // console.log(trunks_list);
            }

            conn.on('Close Socket', function() {});
        });

        var server = http.createServer();
        sock.installHandlers(server, {prefix:'/sockjs'});
        server.listen(3001, '127.0.0.1');

        return null;
    }
}

module.exports = websocket;
//não consegui encontrar uma explicação (tutorial) boa sobre essa parte do websocket  