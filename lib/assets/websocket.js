var http = require('http');
var sockjs = require('sockjs');
var redis = require('redis'),
    redisClient = redis.createClient();

var sock = sockjs.createServer();
sock.on('connection', function(conn) {

    redisClient.on("error", function(err){
        console.log("Error: " + err);
    });
    setInterval( function() {
	    redisClient.get("trunks", function (err, trunks ) {
	        conn.write(trunks);
	        //console.log(trunks);
	    })
        , 3000	});

    conn.on('close', function() {});
});

var server = http.createServer();
sock.installHandlers(server, {prefix:'/trunks'});
server.listen(3001, '127.0.0.1');


//não consegui encontrar uma explicação (tutorial) boa sobre essa parte do websocket  