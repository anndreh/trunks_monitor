var fs = require('fs');
var amqp = require('amqp');
var redis = require('redis');
var websocket = require("./websocket_model");
var mysql = require('mysql');

function getServiceInfo(service) {
	var redisconf = [];
	if (process.env.VCAP_SERVICES) {
		if(service == 'amqp'){
		    conf = JSON.parse(process.env.VCAP_SERVICES);
		    return conf['rabbitmq-2.4'][0].credentials.url;
		}
		if(service == 'redis'){
			redisconf = [conf['redis-2.2'][0].credentials.port, conf['redis-2.2'][0].credentials.host];
		    return redisconf;
		}
		if(service == 'mysql'){
			mysqlconf = {
				host: conf['mysql-5.1'][0].credentials.host, 
				port: conf['mysql-5.1'][0].credentials.port, 
				user: conf['mysql-5.1'][0].credentials.user,
				password: conf['mysql-5.1'][0].credentials.password,  
				database: 'd927acd49db384a859b48dfb7580bf35d'
			};
			return mysqlconf;
		}
	} else {
			if(service == 'amqp'){
			return "localhost";
		}
		if(service == 'redis'){
			redisconf = [6379, 'localhost'];
			return redisconf;
		}
		if(service == 'mysql'){
			mysqlconf ={
				host: 'localhost',
				user: 'root',
				password: '',
				database: 'vonix_pbx'
			}
			return mysqlconf;
		}
	}
}
var connection = amqp.createConnection({host: getServiceInfo('amqp')});
var redisClient = redis.createClient(getServiceInfo('redis')[0], getServiceInfo('redis')[1]);
var mysqlClient = mysql.createConnection(getServiceInfo('mysql'));
mysqlClient.connect(function(err){
	if(err){ console.log('MySQL error:' + err); }
});

// for handler use
testTrunk = {};

connection.on('ready', function(){
	testTrunk = connection.exchange('amq.topic', {autoDelete: true});
	sendMessage();
});

var TrunkModel = require('./trunk_model');
var providers = ['Vivo', 'Embratel'];
var technology = ['SIP', 'DAHDI'];
var current_on = 0;
var current_off = 0;
var trunks_on = [];
var trunks_off = []; // the trunk and the id for remove

// clean database
for (var i in providers){
	redisClient.del("trunk_"+providers[i]);
}

// to publish messages every 2 seconds
function sendMessage(){
	var channel = 0;
	var provider = providers[Math.floor(Math.random() * providers.length)];
	var type = technology[Math.floor(Math.random() * technology.length)];

	if (type == 'DAHDI'){
		channel = Math.floor((Math.random() * (providers.length * 31))+1);
		provider = ''; // provider doesn't exist in this case
	}
	trunks_on.push(new TrunkModel(provider, type, channel));

	// send the online trunk
	testTrunk.publish('trunking.'+trunks_on[current_on].technology, JSON.stringify(trunks_on[current_on]), {contentType: 'application/json', mandatory: true});
	// console.log(trunks_on[current_on]);

	if (trunks_on.length > 2){
		var resTrunk = trunks_on[0].disconnectTrunk(trunks_on, trunks_off);
		// console.log(resTrunk);
		if (resTrunk){
			for (var i in resTrunk.trunk){
				trunks_off.push(resTrunk.trunk[i]);
				trunks_on.splice(resTrunk.index[i], 1);
				current_on--;
				// trunks_on.push(resTrunk.trunk[i]);	
				// send the offline trunk
				testTrunk.publish('trunking.'+trunks_off[current_off].technology, JSON.stringify(trunks_off[current_off]), {contentType: 'application/json', mandatory: true});
				// console.log(trunks_off[current_off] + ' - saiu');
				current_off++;
			}
		}
		// console.log(trunks_on.length + ' - ' + trunks_off.length);
	}

	current_on++;
	// console.log('Handler ativo');
	setTimeout(sendMessage, 3000);
};

// mediator action
connection.on('ready', function(){

    connection.queue('trunks.queue', function(q){
    	q.bind('trunking.#');
        q.subscribe(function(message){
            var trunk = message;
            // console.log(JSON.stringify(trunk) + ' - chegou');
            
    		// DAHDI protocol doesn't have provider name
		    if(trunk.technology == 'DAHDI'){
		    	var query = mysqlClient.query(
					'SELECT * FROM trunkings WHERE ' + trunk.channel + ' BETWEEN first_trunk AND last_trunk',
					function selectCom(err, rows, fields){
						if (err) {
							console.log(err);
						}
						trunk.provider = rows[0].provider;
						// console.log(rows[0]);
					}
				);
				query.on('end', function() {
					updateRedis(trunk);
				});
		    }else{
            	updateRedis(trunk);
            }
        });
    });
});

function updateRedis(trunk){
	var count_trunks;
	var trunk_info;

    redisClient.get("trunk_"+trunk.provider, function (err, result ) {
    	// console.log(result);
    	if(result){
    		result = JSON.parse(result);
	        count_trunks = result.trunks_on ? parseInt(result.trunks_on) : 0;
	        // console.log(JSON.stringify(result) + ' _ ' + JSON.stringify(trunk))
	        
	        switch (trunk.event){
				case 'CHANNEL_START':
					count_trunks += 1;
					// console.log('Mais um');
					break;
				case 'CHANNEL_END':
					count_trunks -= 1;
					// console.log('menos um');
					break;
			}
		}
		count_trunks = count_trunks < 0 ? 0 : count_trunks;
		// put the new value for trunks count in Redis
		trunk_info = {provider: trunk.provider, trunks_on: count_trunks};
		redisClient.del("trunk_"+trunk.provider);
		redisClient.set("trunk_"+trunk.provider, JSON.stringify(trunk_info));
    });

    redisClient.on("error", function(err){
        console.log("Error: " + err);
    });
    
}

websocket.connectSock(redisClient);