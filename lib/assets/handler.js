var fs = require('fs');
var amqp = require('amqp'),
	connection = amqp.createConnection({host: 'localhost'}),
	testTrunk = {};

connection.on('ready', function(){
	testTrunk = connection.exchange('amq.topic', {autoDelete: true});
	testMessage();
});

function testMessage(){

	// READ FROM JSON FILE
	var file = __dirname + '/trunks.json';
	fs.readFile(file, 'utf8', function (err, trunks) {
	  if (err) {
	    console.log('Error: ' + err);
	    return;
	  }
	 
	  trunks = JSON.parse(trunks);
	  //console.log(trunks);

	  testTrunk.publish('trunk.list', JSON.stringify(trunks), {contentType: 'application/json', mandatory: true});
	});

	// console.log('Handler ativo');
	setTimeout(testMessage, 5000);
}