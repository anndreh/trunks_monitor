var app = angular.module('TrunksMonitor', []);

app.controller('MainCtrl', function ($scope) {
    // $scope.widgets = [];
    $scope.providers = [];

    $scope.widgets = [];

    $scope.$on('SockJS.onMessage', function(event, data){
        trunks = angular.fromJson(data);

        // if ($scope.widgets[0].provider == 'Provedor'){ $scope.widgets[0] = trunks }
        if ($scope.providers.indexOf(data.provider) == -1){
            $scope.providers.push(trunks.provider);
            $scope.widgets.push(data);
        }

        // console.log(data.provider + ' - ' + $scope.widgets[0].provider);
        // console.log($scope.widgets.length);

        for (var i in $scope.widgets){

            if ($scope.widgets[i].provider == trunks.provider){
                $scope.widgets[i].trunks_on = trunks.trunks_on;
            }
        }
        $scope.$digest();
        return $scope.widgets;
    });
});

app.directive('container', function ($timeout) {
    return {
        restrict: 'AC',
        scope: {
            model: '='
        },
        template: '<charts><div widget="model" ng-repeat="item in model" widget-model="item"></div></charts>',
        link: function ($scope, $element, $attributes, $controller) {
            var charts = $element.find('charts');
            
            $scope.$watch('model.length', function (newValue, oldValue) {
                if (newValue != oldValue + 1) return; //not an add
                var div = charts.find('div').eq(newValue - 1); //latest div element
            });
        }
    };
});

app.directive('widget', function () {
    return {
        restrict: 'AC',
        scope: {
            widgetModel: '=',
            widget: '='
        },
        replace: true,
        template:
            '<gauge id="{{widgetModel.provider}}" items="activesTrunks">' +
            '</gauge>',
        link: function (scope, element, attrs) {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: element[0],
                    type: 'gauge',
                    height: scope.widgetModel.chartheight,
                    animation: false
                },
                title: {
                    text: ''
                },
                pane: {
                    startAngle: -150,
                    endAngle: 150,
                },
                yAxis: {
                    min: 0,
                    max: 100,

                    minorTickInterval: 'auto',
                    minorTickWidth: 1,
                    minorTickLength: 10,
                    minorTickPosition: 'inside',
                    minorTickColor: '#666',

                    tickPixelInterval: 30,
                    tickWidth: 2,
                    tickPosition: 'inside',
                    tickLength: 10,
                    tickColor: '#666',
                    labels: {
                        step: 2,
                        rotation: 'auto'
                    },
                    title: {
                        text: scope.widgetModel.provider
                    }
                },
                series: [{
                    data: [scope.widgetModel.trunks_on]
                }]
            });
            
            scope.$watch("widgetModel", function (newValue, oldValue, scope) {
                // console.log(scope.widget.length);
                for(var i in scope.widget){
                    if(chart.index == i){
                        // console.log(chart.series[0].points[0]);
                        chart.series[0].points[0].update(newValue.trunks_on); // change the actives trunks info
                        // console.log(newValue);
                    }
                }
            }, true);
        }
    };
});

// connect with the socket
app.run(function($rootScope){
  var sockjs = new SockJS('http://localhost:3001/sockjs');
  
  sockjs.onopen = function() {
    console.log('open');
  };
  sockjs.onmessage = function(e) {
    var data = angular.fromJson(e.data);
    // console.log(providers);
    $rootScope.$broadcast('SockJS.onMessage', data);
  };
  sockjs.onclose = function() {
    console.log('close');
  };
});