function TrunksCtrl($scope) {
  $scope.trunks = [0, 'Provedor'];
  $scope.providers = [];

  $scope.$on('SockJS.onMessage', function(event, data){
    trunks = angular.fromJson(data);

    if ($scope.providers.indexOf(data.provider) == -1){
      $scope.providers.push(data.provider);
    }

    // update text infos
    // $scope.total = 100;
  	$scope.qt_on = trunks.trunks_on;
  	$scope.provider = trunks.provider;

    // update gauge graph infos			
    $scope.trunks[0] = [trunks.trunks_on];
    $scope.trunks[1] = trunks.provider;
    $scope.$digest();
    // console.log($scope.trunks);
    return $scope.trunks;
  });
  
  $scope.activesTrunks = $scope.trunks;
  // console.log($scope.trunks);
};

angular.module('TrunksMonitor', [])
  .directive('hcGauge', function () {
  return {
    restrict: 'C',
    replace: true,
    scope: {
      items: '='
    },
    controller: function ($scope, $element, $attrs) {
      // console.log(2);

    },
    template: '<div id="container" style="margin: 0 auto">not working</div>',
    link: function (scope, element, attrs) {
      // console.log(3);
      var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'container',
          type: 'gauge'
        },
        title: { text: 'Troncos ativos' },
        pane: {
            startAngle: -150,
            endAngle: 150,
        },
        yAxis: {
            min: 0,
            max: 100,
            
            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',
    
            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: 'Embratel'
            }        
        },
        series: [{
            data: [0]
        }]
      });

      scope.$watch("items", function (newValue, oldValue, scope) {
        // console.log(newValue);
        chart.series[0].points[0].update(newValue[0]); // change the actives trunks info
        // chart.yAxis[0].setExtremes(null, newValue[1]); // change the max size of gauge
        chart.yAxis[0].setTitle({text: newValue[1]});
      }, true);
      
    }
  }
})
// connect with the socket
.run(function($rootScope){
  var sockjs = new SockJS('http://localhost:3001/sockjs');
  
  sockjs.onopen = function() {
    console.log('open');
  };
  sockjs.onmessage = function(e) {
    var data = angular.fromJson(e.data);
    // console.log(providers);
    $rootScope.$broadcast('SockJS.onMessage', data);
  };
  sockjs.onclose = function() {
    console.log('close');
  };
});