class TrunksSimmulatorsController < ApplicationController
	# GET /trunks_simulators
	def index
		@trunks = TrunksSimmulator.find_by_id(1)
		respond_to do |format|
			format.html
		end
	end

	def find
		@trunks = TrunksSimmulator.new
	end

	def update
		@trunks = TrunksSimmulator.find(params[:id])
	
		respond_to do |format|
			if @trunks.update_attributes(params[:trunks_simmulator])
				# define the qt of offline trunks
				total_off = @trunks.total - @trunks.qt_on
				new_trunk = {"total" => @trunks.total,
							"qt_on" => @trunks.qt_on,
							"qt_off" => total_off }

				# save a json file with trunks simmulation
				File.open('lib/assets/trunks.json', 'w') { |f| f << JSON.generate(new_trunk) }

				# update qt_off in db to show in page
				@trunks.update_column(:qt_off, total_off)

				format.html { redirect_to( trunks_simmulators_url,
						:notice => 'Trunks simmulator successfully updated.')}
			else
				format.html { render :action => "error" }
			end
		end
	end
end
