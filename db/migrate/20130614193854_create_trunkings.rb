class CreateTrunkings < ActiveRecord::Migration
  def change
    create_table :trunkings do |t|
      t.string :provider, :null => false, :limit => 50
      t.integer :first_trunk
      t.integer :last_trunk

      t.timestamps
    end
  end
end
