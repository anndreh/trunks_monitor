class CreateTrunksSimmulators < ActiveRecord::Migration
  def change
    create_table :trunks_simmulators do |t|
      t.integer :total
      t.integer :qt_on
      t.integer :qt_off

      t.timestamps
    end
  end
end
