# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

TrunksSimmulator.create(total: 50, qt_on: 10, qt_off: 40)
Trunking.create([
	{provider: 'Vivo', first_trunk: 1, last_trunk: 31},
	{provider: 'Embratel', first_trunk: 32, last_trunk: 63}
])