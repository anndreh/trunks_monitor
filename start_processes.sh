#!/bin/bash

# cd /Users/anndreh/Documents/trunks_monitor

trap 'kill $(jobs -p)' EXIT

# Starts the Rails server
# echo "Starting Rails server"
# rails s &
# sleep 3

# echo "Servers started"
# sleep 1

cd script

# Starts the handler
echo "Starting Handler"
node handler.js &
sleep 1

# Starts the Mediator
echo "Starting Mediator"
node mediator.js &
sleep 1

# Starts the websocket
echo "Starting Websocket"
node websocket.js &
sleep 1

# Waiting for stop
read -p "Press [Enter] key to stop all services and then CTRL + C to close"